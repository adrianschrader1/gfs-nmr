\section{Kernspinresonanzspektroskopie}
\label{nmr}
Die Kernspinresonanz\footnote{engl. \glqq{}nuclear magnetic resonance\grqq{}, kurz NMR} ist eine fundamentale Eigenschaft der Atomkerne und lässt sich in allen Molekülen nachweisen, die kernspinaktive Atome wie \ce{^1H} enthalten.  Verschiedene Effekte, die das lokale Magnetfeld am Kern bzw.\ die Kopplung von Kernen untereinander lassen sich durch die \emph{Kernspinresonanzspektroskopie} messen, um Rückschlüsse auf die Eigenschaften und vor allem auf die Struktur des Moleküls zu ziehen.
Gerade die Abschirmungseffekte, die auf die Elektronenhülle zurückzuführen sind, machen Aussagen über Elektronegativiät, Bindungstypen, sowie intra- und intermolekulare Kräfte möglich.

\subsection{Technischer Aufbau}
\label{nmr:structure}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{img/nmr_structure}\label{img:nmr_structure}
  \caption{Schematischer Aufbau eines einfachen NMR-Spektroskop (Quelle:  \url{https://de.wikipedia.org/wiki/Datei:NMR-Spektrometer.svg}, 08.01.16)}
\end{figure}

Mit der NMR-Spektroskopie werden hauptsächlich Flüssigkeiten untersucht, in denen die zu untersuchenden Stoffe gelöst sind, um Störeffekte bei eventueller Kristallbildung zu umgehen. Dennoch werden auch Polymere und andere Feststoffe mit dieser Technik untersucht, denn auch diese Störeffekte geben Aufschluss auf die Eigenschaften der Probe und können ausgewertet werden.

\subsubsection*{Versuchsaufbau}
In jeder Methode der NMR-Spektroskopie wird durch Elektromagneten ein mehrere Tesla starkes, homogenes B-Feld aufgebaut und eventuell mit kleineren Spulen stabilisiert, um Inhomogenitäten auszugleichen. Einige Spektrometer verwenden mit flüssigem Stickstoff oder Helium gekühlte Supraleiter als Spulenmaterial, um noch höhere Feldstärken zu erreichen.

Ein zweites Spulensystem soll die Resonanzfelder der Probe messen und die Hochfrequenzpulse senden. Durch Anregung und Relaxation der Kerne verändert sich das Magnetfeld der Kerne ständig ($ \frac{dB}{dt} \not= 0 $), was zu einer magnetischen Flussdichteänderung ($ \frac{d\phi}{dt} = n \cdot A \cdot \frac{dB}{dt} $) führt, die einen Induktionsstrom in den senktecht zum statischen Magnetfeld angebrachten Spulen induziert.
Das Probenvolumen von wenigen Millilitern wird in einem speziellen Gefä\ss{} in diese Spule eingebracht und gesichert.
Der Aufbau ist in Abbildung~\ref{img:nmr_structure} zu sehen.

Der offensichtlichsten Störfaktoren sind die anderen Moleküle und spinaktiven Atome im Lösungsmittel. Für die hier vorgestellte \ce{^1H}-NMR Methode wird Deuterium verwendet, das mit einer Spinquantenzahl von 1 andere Resonanzfrequenzen aufweist, die leicht abzugrenzen sind.

\begin{figure}[b]
  \centering
  %\includegraphics[width=\linewidth]{img/nmr_tms}\label{img:nmr_tms}
  \setatomsep{2.5em}
  \schemestart[90]
   \chemname{\chemfig{Si (-[2]CH3) (-[:-140]CH3) (<[:-65]CH3) (<:[:-20]CH3) }}{Trimethylsilan}\qquad\qquad
   \chemname{\chemfig{[0]C (-[::90]CH3) (-[::-140]CH3) (<[::-65]CH3) (<:[::-20]CH3) }}{Neopentan}
  \schemestop{}
  \caption{Strukturformel der Normierungssubstanz Tetramethylsilan (links) und Neopentan (rechts)\label{img:nmr_tms}}
\end{figure}

\subsubsection*{Versuchsablauf}
Moderne Spektroskopiesysteme verwenden Radiosignale mit einem breiten Frequenzband, um die Kerne anzuregen. Damit werden alle Kerne, deren Lamorfrequenz in diesem Band liegt, gleichzeitig angeregt. Mit der selben Spule kann danach das Signal der Überlagerung der Larmor-Präzessionen aller Kerne aufgezeichnet werden. Mit Hilfe einer FFT\footnote{Abkürzung für \glqq{}Fast Fourier Transform\grqq{}; zu deutsch \glqq{}schnelle Fourier-Transformation\grqq{}} kann diese Überlagerung in die einzelnen Frequenzen aufgespalten werden.

\subsection{Chemische Verschiebung}
\label{nmr:shift}

Auch die Elektronenhülle jedes Kerns ist von den Effekten der Magnetfelder betroffen. Analog zu in ein Magnetfeld eingebrachte Metallplatten werden auch in den Elektronenhüllen \glqq{}Ringströme\grqq{} erzeugt, die selbst ein Magnetfeld aufbauen. Dieses durch den \glqq{}Induktionsstrom\grqq{} aufgebaute B-Feld interferiert mit dem statischen Magnetfeld, sodass sich das lokale Feld für den Kern abschwächt oder verstärkt.

Da die Resonanzfrequenz proportional zum lokalen Magnetfeld ist, wird das Frequenzbild des Kerns durch seine Elektronenhülle verschoben. Dieser Effekt der chemischen Verschiebung $ \delta $ ist für viele funktionelle Gruppen charakteristisch und wird als relativer Frequenzabstand zu einer Normierungssubstanz gemessen und meist in ppm angegeben. Die meisten Spektrogramme sind über der chemischen Verschiebung angegeben, um die Vergleichbarkeit zwischen verschiedenen Modellen, Geräten und Messmethoden zu ermöglichen.

Zur Normierung der Verschiebung wird der Probelösung zusätzlich eine Standardsubstanz zugegeben, deren Resonanzeigenschaften eindeutig bekannt sind.
Eines der verbreitetsten Substanzen ist das Tetramethylsilan, kurz TMS (siehe Abbildung~\ref{img:nmr_tms}). Alle Wasserstoffe sind gleichwertig, sodass nur eine einzelne Spitze im Spektrum auftaucht. Im Periodensystem steht Silizium eine Periode unter Kohlenstoff; seine Elektronegativität ist also etwas geringer. Trotzdem weist es ein zu Kohlenstoff vergleichbares Bindungsverhalten auf.
Damit taucht der Peak von TMS durch die geringere Abschirmung rechts von Neopentan (2,2-Dimethylpropan) auf.

\begin{align}
  \delta = \frac{\nu_\text{Probe} - \nu_\text{Referenz}}{\nu_\text{Referenz}}
\end{align}

Zur Einordnung der funktionellen Gruppen in diese Skala kann mit der lokalen Elektronendichte und den Partialladungen argumentiert werden. Die Verschiebung von Alkanen ist demnach gering, da kaum Elektronen delokalisiert wurden oder Partialladungen entstehen konnten.
Durch Einfügen von Sauerstoffatomen oder Halogenen können Partialladungen aufgebaut werden, die einen Elektronenüberschuss am Sauerstoff und einen Elektronenmangel an den benachbarten Kohlenstoffen und damit auch an den angrenzenden Wasserstoffen hervorrufen. Die Intensität der Verschiebung nimmt dabei mit Entfernung zum elektronegativen Atom stark ab. Durch die geringere Elektronendichte nimmt auch die Abschirmung des Kerns ab; die Resonanzfrequenz wird grö\ss{}er.

Ester-, Ether-, Carbonyl- und Carboxylgruppen beziehen aus dieser Eigenschaft ihre Verschiebung. Die Verschiebung durch eine Auswahl funktioneller Gruppen ist in Abbildung~\ref{nmr:shift} gezeigt.

\subsection{J-Coupling}
\label{nmr:jcoup}

Auch verschiedene Kopplungseffekte können die Signale der Wasserstoffe verändern und liefern weitere Hinweise auf die Molekülstruktur. Hier wird nur das J-Coupling aufgeführt, weil es die weitreichendsten Folgen für die Signalauswertung hat.

Da die Wasserstoffkerne einen magnetischen Dipol haben, beeinflussen sie dadurch auch ihre bindenden Elektronenpaare, die diese Veränderung zu einem benachbarten Wasserstoff weitertragen können. Es entstehen dabei zwei herausragende Effekte im Signal: Die Aufspaltung des Signals durch benachbarte Wasserstoffatome und die Steigerung der Intensität.

\subsubsection*{Multiplizität}
Durch diesen Kopplungseffekt spaltet sich das Signal in so genannten Multipletts auf. Diese Signale enthalten mehrere Spitzen, die zu den Seiten nach der Binominalverteilung in Intensität verlieren. Die Anzahl der Spitzen ist immer um ein höher als die Anzahl der benachbarten Wasserstoffe. Die gleichartigen Wasserstoffe haben keinen Effekt auf die Multiplizität.

Die Multipletts bekommen in der Auswertung meist Namen und Abkürzungen. Eine einzelne Spitze ist ein Singulett, zwei Spitzen ein Duplett, drei Spitzen ein Triplett, usw.

\subsubsection*{Intensität}
Alle gleichartigen Wasserstoffatome haben das gleiche Resonanzverhalten. Die einzelnen Signale überlagern sich und steigern damit die Intensität des Multipletts. Über die Integralfunktion, die in vielen Signalplots mit ausgegeben wird, lässt sich die Anzahl der gleichartigen Wasserstoffe durch das Vergleichen mit anderen Multipletts bestimmen. Die Flächeninhalte der einzelnen Multipletts sind immer Vielfache voneinander.

\subsection{Anwendungsbeispiel}

\subsubsection*{Aufgabenstellung}
In Abbildung~\ref{img:butanon} ist das Ergebnis einer \ce{^1H}-NMR-Spektroskopie angeben. Die Elementaranalyse ergab, dass das Molekül ein Kohlenwasserstoff aus Wasserstoff, Kohlenstoff und Sauerstoff ist.

Der Flächeninhalt unter den Signalen B und C ist $\frac{3}{2}$ mal so gro\ss{} wie der Flächeninhalt unter dem Signal A.

\subsubsection*{Auswertung}
Aus den oben beschriebenen Prinzipien ergeben sich die Angaben aus Tabelle~\ref{tbl:multipl} für die einzelnen Multipletts.

\begin{table}[b]
  \centering
  \caption{Daten aus dem auszuwertenden NMR-Signal}
  \begin{tabular}{c c c c c}
     & \textbf{Intensität} & \textbf{Nachbarn} & \textbf{$\delta$} & \textbf{Gruppe}\\ \hline
    A & 2 & 3 & 2,4ppm & \ce{-CH2-} \\
    B & 3 & 0 & 2,1ppm & \ce{-CH3} \\
    C & 3 & 2 & 1,0ppm & \ce{-CH3} \\
  \end{tabular}\label{tbl:multipl}
\end{table}

Durch die Form der Multipletts lassen sich einige funktionelle Gruppen ausschlie\ss{}en. Mit den zur Verfügung stehenden Elementen können die Wasserstoffe entweder \ce{H-H}, \ce{C-H} oder \ce{O-H} Bindungen entstehen. Der erste Fall ist ausgeschlossen, weil Wasserstoffgas in der Probe auffallen und keine Multipletts bilden würde. Für eine Hydroxylgruppe müsste Multiplett B, das einzige ohne direkte Nachbarn, von einem einzelnen Wasserstoff stammen. Da seine Intensität aber auf drei gleiche Wasserstoffe schlie\ss{}en lässt, ist auch dieser Fall zu vernachlässigen. Alle Wasserstoffe scheinen mit Kohlenstoffen verbunden zu sein. Daraus lässt sich mit der Intensität die Art von $CH_n$ Gruppe bestimmen.

Laut Elementaranalyse muss aber noch mindestens ein Sauerstoffatom verbaut werden. Die chemische Verschiebung von A und B ist nur mit einer einzigen Ketogruppe \ce{-CO-} zu erklären. Ether \ce{-O-} verursachen Verschiebungen von 3,5 ppm, Methyester \ce{-CO-O-CH3} von 3,77 ppm, Ethyester \ce{-CO-O-CH2-CH3} von 4,12; Carboxyl- und Aldehydgruppen benötigen einen einzelnen Wasserstoff ohne Nachbarn.~\cite{shift}

Wenn man die Bausteine nach ihrer Verschiebung zusammensetzt bleibt nur noch eine mögliche Struktur übrig, die in Abbildung~\ref{img:struc_butanon} gezeigt ist. Butan-2-on hat drei äquivalente Wasserstoffe und eine Ketogruppe. Höherwertige Kohlenwasserstoffe sind nicht möglich, weil sonst die Verschiebung durch Dreifachbindungen oder funktionelle Gruppen höher wäre.

\begin{figure}
  \centering
  \setatomsep{2.5em}
  \schemestart[90]
   \chemname{
     \chemfig{C (-[4]H) (-[2]H) (-[6]H) -C (-[2]H) (-[6]H) -C (=[2]O) -C (-[0]H) (-[2]H) (-[6]H) }
   }{Butan-2-on}
  \schemestop{}
  \caption{Strukturformel zum auszuwertenden Spektrum\label{img:struc_butanon}}
\end{figure}

\begin{figure*}
  \centering
  \includegraphics[width=\linewidth]{img/nmr_shift}
  \caption{Zeigt die Verschiebungsbereiche ausgewählter funktioneller Gruppen. Das verschobene Signal gehört jeweils zum rot hervorgehobenen Wasserstoff. (Quelle: \url{http://www.process-nmr.com/images/nmr1.h5.gif}, 08.01.16)\label{img:nmr_shift}}
\end{figure*}
\begin{figure*}
  \centering
  \includegraphics[width=\linewidth]{img/nmr_butanon}
  \caption{(Quelle: \url{http://www.ces.clemson.edu/IMMEX/backup/asif/Spectra/Case9/images/exONEhn.gif}, 08.01.16, Strukturformel entfernt)\label{img:butanon}}
\end{figure*}
