\section{Grundlagen}
\label{bsc}
Es gibt zwei verschiedene Ansätze, um die Hintergründe des Kernspinresonanzverhaltens von Atomkernen zu erläutern. Das Modell der Quantenmechanik entwickelt aus Operatoren der Wellenfunktion das Verhalten eines einzelnen Kerns und dessen Verhalten in statischen Magnetfeldern, während der klassische Erklärungsversuch zwar nicht an die mathematische Gründlichkeit herankommt, jedoch auf der Intuition des klassischen Physikers basiert. Diese klassische Methode betrachtet die Gesamtheit aller Atomkerne im betrachteten System, um mithilfe der Statistik die makroskopischen Effekte zu erklären.

Zum besseren Verständnis beschreibt dieses Paper nicht die mathematischen Hintergründe, sondern argumentiert mit Annahmen aus der Quantenmechanik, die es selbst nicht erklären kann. Oft werden an solchen Stellen Vergleiche zur klassischen Physik und zu den Gesetzten der Elektrostatik gezogen, obwohl diese in den Grö\ss{}enordnungen der Elementarladungen nicht mehr anwendbar sind. Sie vermitteln trotzdem eine Intuition für die quantenmechanischen Annahmen und verdeutlichen als Annahmen die Argumentation.

\subsection{Kernspin}
\label{bsc:spin}
Neben Masse und Ladung tragen Elektronen, Neutronen und Protonen die Eigenschaft des sogenannten Spins $\vec{I}$, der mit einem intrinsischen Drehimpuls vergleichbar ist. Als quantenmechanische Grö\ss{}e kann der Spin nur in diskreten Beträgen und Richtungen auftreten, die von seiner Spinquantenzahl $I$ bestimmt werden. Protonen haben eine Spinquantenzahl von $ I = \frac{1}{2} $ und weisen so die zwei Spinzustände \glqq{}spin up\grqq{} ($ m=\frac{1}{2} $) und \glqq{}spin down\grqq\ ($m=-\frac{1}{2}$) auf. $m$ beschreibt dabei die relevante magnetische Quantenzahl.~\cite[S. 2]{schlemm08}

Auch Atomkerne sind aus diesen Teilchen aufgebaut und können somit auch einen Spin tragen, wenn die Spins der Protonen und Neutronen nicht gepaart werden können. Generell haben Kerne mit ungerader Anzahl von Protonen oder Neutronen einen von null verschiedenen Spin.

Die häufigsten Isotope, die für die nachfolgenden Mechanismen eingesetzt werden können weil sie ungepaarte Nukleonen enthalten sind \ce{^{1}H}, \ce{^{13}C}, \ce{^{15}N} oder \ce{^{17}O}. Die gebräuchlichen Atome der organischen Chemie werden bei den Analysemethoden durch ihre benötigten kernspinaktiven Isotope ausgetauscht; jedoch werden wir sehen, dass es meist ausreicht den Spin einer einzelnen Atomgruppe, des Wasserstoffs, zu betrachten.

Der Kernspin ist deshalb relevant, weil Atomkerne mit einem Kernspin ungleich null ein magnetisches Dipolmoment $\vec{\mu}$ aufweisen. Aus der klassischen Physik kennt man diesen Effekt von geladenen Kugeln, die auch einen magnetischen Dipol aufbauen, wenn sie um eine Achse rotieren. Da der Dipolmoment vom Spin abhängig ist, tritt auch er gequantelt auf.

\subsection{Polarisierung}
\label{bsc:polarisation}
Die als Dipol fungierenden Atomkerne werden nun in ein starkes, homogenes, statisches Magnetfeld $B_0$ gesetzt dessen Feldlinien parallel zur z-Achse verlaufen. Wie man es vom Verhalten von Stabmagnet in diesem Umfeld vermuten würde, wollen sich die Kerne so ausrichten, dass ihre magnetischen Momente so parallel wie möglich zum statischen Feld liegen. Dieser Zustand hat die geringste potentielle Energie im Feld. Versucht man die Kerne (oder die Stabmagnete in der Analogie) zu drehen, muss Arbeit geleistet werden; der um $ 180^\circ $ gedrehte Zustand ist also derjenige mit der höchsten potentiellen Energie.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{img/zeeman}\label{img:zeeman}
  \caption{Verschiebung der Energieniveaus bei Vergrö\ss{}erung des Magnetfeldes. Die gestrichelten Linien verdeutlichen die Zugehörigkeit zu den Spinquantenzahlen: schwarz $\rightarrow{} I=\frac{1}{2} $, blau $ \rightarrow{} I=\frac{3}{2} $, rot $\rightarrow{} I=\frac{5}{2} $. Die Verschiebung der Linien dient nur der deutlicheren Darstellung. (Quelle: \fullcite{kaseman})}
\end{figure}

\subsection{Larmor-Präzession}
\label{bsc:larmor}
Wenn eine äu\ss{}ere Kraft auf die Achse einen rotierenden Kreisel ausgeübt wird, folgt dieser keiner geraden Linie; seine Kreiselbahn beschreibt eine Krümmung. Bei konstant ausgeübter Kraft beginnt der Kreisel eine Präzessionsbewegung um die neue Rotationsachse, der er sich langsam annähert.

Diese Intuition ist auch auf den Kernspin übertragbar. Durch das polarisierende Magnetfeld präzessiert der Kernspin um die Achse des statischen Magnetfelds. Doch durch die gequantelte Natur des Kernspins, kann sich dieser nur auf diskreten, äquidistanten Bahnen bewegen wie in Abbildung~\ref{img:lamor} gezeigt. Protonen haben mit zwei möglichen Spinzahlen auch zwei mögliche Präzessionsbahnen.

\begin{figure*}
  \centering
  \includegraphics[width=\linewidth]{img/lamor.jpg}
  \caption{Betrag und Richtung des Eigendipolmoments (Spins) auf gequantelten Präzessionsbahnen. (Quelle: \fullcite{kaseman})\label{img:lamor}}
\end{figure*}

Durch diese Larmor-Präzession kann der einmalig ausgelenkte Kern nie direkt parallel zum Feld stehen, sondern wird in immer kleiner werdenden diskreten Radien um die z-Achse kreisen. Der Vektor des magnetischen Dipols hat durch diese Präzessionsbewegung nun nicht nur eine z-Komponente, sondern auch Anteile in x und y-Richtung, die mit zum Feld orthogonal angebrachten Spulen in den Anwendungsfällen gemessen werden können.

\subsection{Zeeman-Effekt}
\label{bsc:zeeman}
Der Kernspin kann wie in Abschnitt~\ref{bsc:spin} beschreiben, verschiedene gequantelte Orientierungen einnehmen. Je nach Richtung des Spins wird jedoch auch der an den Spin gekoppelte magnetische Dipolmoment verschoben, der durch das statische Feld möglichst parallel zur z-Achse stehen möchte. Jeder Spinzustand bekommt so ein von Winkel abhängiges, gequanteltes Energieniveau. Im Energiediagramm aus Abbildung~\ref{img:zeeman} sieht man, wie die Aufspaltung der Energieniveaus für verschiedene Spinquantenzahlen aussieht. 

Ohne Magnetfeld sind die Spins in allen Zuständen gleich verteilt. Durch das statische Magnetfeld entsteht jedoch eine Energiedifferenz zwischen den Bahnen der Larmor-Präzession. Diese ist verhältnismä\ss{}ig gering und die thermische Energie reicht aus um einige Spins in die antiparallel Position zu heben. Die statistische Verteilung aller Teilchen des Systems wird zu Gunsten der parallelen Stellung verschoben. Dadurch magnetisiert sich die Probe, da sich die magnetischen Momente nicht mehr gegenseitig aufheben.

\subsection{Resonanz}
\label{bsc:resonance}

Eine Veränderung des Spinzustands kann durch elektromagnetische Wellen ausgelöst werden. Dabei ist gerade der Anteil der magnetischen Wechselfelder entscheidend, sodass im Anwendungsfall oft Spulen mit Wechselströmen zur Anregung der Kerne verwendet werden.

Da die Spinzustände gequantelt sind, muss die Welle genau die richtige Energie haben, um die Energiedifferenz zwischen den Zuständen zu überwinden. Diese ist proportional zur magnetischen Spinzahl $m$ und dem statischen Magnetfeld $B_0$. $\gamma$ ist die kernspezifische gyromagnetische Konstante und $\hbar = \frac{h}{2 \pi}$ die verminderte Plankkonstante.~\cite[]{kaseman}

\begin{align}
  E &= - \gamma \cdot m \cdot \hbar \cdot B_0 && \Delta m = 1 \notag \\
  \Delta E &= \gamma \cdot \hbar \cdot B_0 &&
\end{align}

Die Energiedifferenz muss jetzt durch das in der Anregung verbrauchte Photon bereitgestellt werden, dessen Energie proportional zu seiner Frequenz ($ E = h \cdot \nu$) ist.

\begin{align}
  h \cdot \nu &= \gamma \cdot \frac{h}{2 \pi} \cdot B_0 \notag \\
  \nu &= \frac{\gamma}{2 \pi} \cdot B_0
  \label{alg:resonance}
\end{align}

Durch das Einstrahlen der Wellen im Radiobereich lassen sich die Spins einzelner Kerne umdrehen, sodass sich die Summe aller magnetischen Dipole im System langsam von der z-Achse in Richtung xy-Ebene bewegt. Dieser Effekt kann durch Induktionsspulen in xy-Ebene nachgewiesen werden.
Die Energieaufnahme des Kerns ist ein Resonanzphänomen, da die eingestrahlte Radiofrequenz gleich der Lamorfrequenz ist mit der die Spins das Magnetfeld umkreisen.
